//
//  ViewController.swift
//  Light
//
//  Created by Vlad on 2/14/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import AVFoundation
import UIKit

class ViewController: UIViewController {
    var isLightOn: Bool = true
    
    @IBOutlet weak var button: UIButton!
    
    @IBAction func handleToggleLight(_ sender: UIButton) {
        isLightOn = !isLightOn
        
        updateView(toggle: isLightOn)
    }
    
    func updateView(toggle: Bool) {
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        
        if let dev = device, dev.hasTorch {
            view.backgroundColor = .black
            
            do {
                try dev.lockForConfiguration()
                dev.torchMode = toggle ? .on : .off
                dev.unlockForConfiguration()
            } catch {
                print("Error")
            }
        } else {
            view.backgroundColor = isLightOn ? .white : .black
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateView(toggle: isLightOn)
    }


}

